---
layout: markdown_page
title: "Create Team"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Create Team
{: #create}

The responsibilities of this team are described by the [Create product
category](/handbook/product/categories/#dev).
